<?php
App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');

class Profile extends AppModel {
	public $uses = array("Profile");
    public $validate = array(
        'username' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'A username is required'),
            'isUnique' => array(
            	'rule'=> 'isUnique',
            	'message'=>'This username is already aquired')
            ),
            
            
        'password' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'A password is required'
            ),
            'maxlength' => array(
            	'rule'=> array('maxlength',20),
            	'message' => 'Maximum length is only 20 Characters'
            	)
            ),
        'firstname' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'A firstname is required'),
            ),

        'lastname' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'A firstname is required'),
            ),
        'Gender_id'=> array(
            'required' => array(
                'rule' => array(
                    'message'=> 'Gender is required'
                    ),
               
            ),
        'mail'=> array(
            'required' => array(
                'rule' => array(
                    'message'=> 'maile is required'
                    )
                ),
             'isUnique' => array(
                'rule'=> 'isUnique',
                'message'=>'This mail is already aquired')
                )
            ),
        );

    public function beforeSave($options = array()) {
    if (isset($this->data[$this->alias]['password'])) {
        if($this->data[$this->alias]['password']==$this->data[$this->alias]['Confirm password'])
        {
            $passwordHasher = new BlowfishPasswordHasher();
            $this->data[$this->alias]['password'] = $passwordHasher->hash(
            $this->data[$this->alias]['password']);
        }
        else{
            echo 'Password Mismatch';
        }
        
    }
    return true;
	}
		public $hasMany = array(
	    'UserFriends' => array(
		'className' => 'Friendrelation',
		'foreignKey' => 'profile_id'
		),
        'photo' => array(
        'className' => 'Photo',
        'foreignKey' => 'profile_id'
        ),
        'Comment' => array(
        'className' => 'Comment',
        'foreignKey' => 'profile_id')
	);
}


?>

