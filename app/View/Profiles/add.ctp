<?php echo $this->Form->create('Photo',array('type'=>'file')); ?>
  <fieldset>
    <legend><?php echo __('Add Photo'); ?></legend>

  <?php

    echo $this->Form->input('Upload', array('action' => 'add', 'type' => 'file'));
  ?>
  </fieldset>
<?php echo $this->Form->end(__('Submit')); ?>