<h1>Register </h1>
<?php
	echo $this->Form->create('Profile');
	echo $this->Form->input('username');
	echo $this->Form->input('firstname');
	echo $this->Form->input('lastname');
	echo $this->Form->input('mail',array('type' => 'email'));
	echo $this->Form->input('password');
	echo $this->Form->input('Confirm password',array('type'=>'password'));

	$options = array('1' => 'Male', '2' => 'Female');
	$attributes = array('legend' => false);
	echo $this->Form->radio('gender_id', $options);
	echo $this->Form->end('Register');
	?>
