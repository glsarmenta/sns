<?php
// so we use the paginator object the shorter way.
// instead of using '$this->Paginator' everytime, we'll use '$paginator'
$paginator = $this->Paginator;
 
if($Profile){
 
    //creating our table
    echo "<table>";
 
        // our table header, we can sort the data user the paginator sort() method!
        echo "<tr>";
         
            // in the sort method, ther first parameter is the same as the column name in our table
            // the second parameter is the header label we want to display in the view
            echo "<th>" . $paginator->sort('id', 'ID') . "</th>";
            echo "<th>" . $paginator->sort('firstname', 'Firstname') . "</th>";
            echo "<th>" . $paginator->sort('lastname', 'Lastname') . "</th>";
            echo "<th>" . $paginator->sort('username', 'Username') . "</th>";
            echo "<th>" . ('Action') . "</th>";
        echo "</tr>";
         
        // loop through the user's records
        foreach( $Profile as $user ){
            echo "<tr>";
                echo "<td>{$user['Profile']['id']}</td>";

                echo "<td>{$user['Profile']['firstname']}</td>";
                echo "<td>{$user['Profile']['lastname']}</td>";
                echo "<td>{$user['Profile']['username']}</td>";
                echo '<td>'.$this->Form->postLink('Delete',array('controller'=>'admins','action'=>'delete',$user['Profile']['id'] ),array(
                	'confirm' => 'Are you sure you want to delete this user?')).'  '.
                $this->HTML->Link('Edit',array('controller'=>'admins','action'=>'edit',$user['Profile']['id'] )). '</td>';
            echo "</tr>";
        }
         
    echo "</table>";
 
    // pagination section
    echo "<div class='paging'>";
 
        // the 'first' page button
        echo $paginator->first("First");
         
        // 'prev' page button, 
        // we can check using the paginator hasPrev() method if there's a previous page
        // save with the 'next' page button
        if($paginator->hasPrev()){
            echo $paginator->prev("Prev");
        }
         
        // the 'number' page buttons
        echo $paginator->numbers(array('modulus' => 2));
         
        // for the 'next' button
        if($paginator->hasNext()){
            echo $paginator->next("Next");
        }
         
        // the 'last' page button
        echo $paginator->last("Last");
     
    echo "</div>";
     
}
 
// tell the user there's no records found
else{
    echo "No users found.";
}

?>