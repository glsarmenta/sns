<?php

App::uses('AppController','Controller');
class AdminsController extends AppController{
	public $components = array('Paginator');


	public $uses = array("Profile","Friendrelation");

	public function index(){
	$this->paginate = array(
       // 'conditions' => array('Profile.id !=' => '6'),
        'limit' => 3,
        'order' => array('id' => 'asc')
    );
     
    // we are using the 'User' model
    $users = $this->paginate('Profile');
     
    // pass the value to our view.ctp
    $this->set('Profile', $users);
	//$this->set('profile',$this->Profile->find('all'));


}
public function edit($id){
	$data = $this->Profile->findById($id); 
	if($this->request->is(array('post','put') ))
	{
		//$this->set('profile',$this->Profile->findById($id));
		$this->Profile->id= $id;
		if($this->Profile->save($this->request->data)){
			$this->Session->setFlash('The user has been updated');
			$this->redirect('index');
		}

	}
	$this->request->data = $data;
}
public function delete($id){
	$this->Profile->id = $id;
	if($this->request->is(array('post','put'))){
		if($this->Profile->delete()){
			$this->Session->setFlash('The user has been deleted');
			$this->redirect('index');

		}
	}
	
}
}

?>