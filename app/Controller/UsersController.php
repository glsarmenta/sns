<?php

App::uses('AppController','Controller');

class UsersController extends AppController{
		public $uses = array("Profile","User");
		

	public function beforeFilter() {
    parent::beforeFilter();
    // Allow users to register and logout.
    $this->Auth->allow('add', 'logout','register');
}

public function login() {
    if ($this->request->is('post')) {
        if ($this->Auth->login()) {
        	if($this->Auth->user('accountType')==2)
        	{
        		$this->redirect(array(
							    'controller' => 'admins',
							    'action' => 'index')
        		);
        	}
        	else{
        	//print_r($this->Auth->user());
            return $this->redirect($this->Auth->redirectUrl());
            }
        }
        else {
        $this->Session->setFlash(__('Invalid username or password, try again'));
        }
    }
}

public function logout() {
    return $this->redirect($this->Auth->logout());
}
	public function register(){

		if($this->request->is('post'))
		{
			$this->Profile->create();
			//$this->request->data['Profile']['password'] = AuthComponent::password($this->request->data);
			if($this->Profile->save($this->request->data)){

				return $this->redirect(array('action' => 'login',
					'controller' =>'users'));
		}
		else {
			$this->Session->setflash(__('The user could not be saved'));

		}

		}
	}
}


?>